# Jira2Gitlab Migration Tool

The Jira2Gitlab Migration Tool is designed to facilitate a smooth transition of projects, issues, and configurations from Jira to GitLab. It aims to streamline the migration process, ensuring data integrity and minimizing workflow disruption.

## Acknowledgments

This tool is based on a fork of [Jira2gitlab](https://github.com/swingbit/jira2gitlab), developed by [swingbit](https://github.com/swingbit). We express our gratitude to the original creators for their foundational work. Our version builds upon the original by introducing the following enhancements:

- **Enhanced Performance:** Optimizations for faster data processing and migration.
- **Improved User Mapping:** Advanced mapping logic to handle complex user scenarios.
- **Custom Field Support:** Expanded support for a wider range of Jira custom fields.
- **Updated Documentation:** Comprehensive guides and updated usage instructions.
- **Additional Features:** List any other significant improvements or additional features.

We encourage users to compare both tools to best meet their migration needs and to contribute to further development.

## Features

- **Project Migration:** Migrates Jira projects to GitLab groups and projects.
- **Issue Transfer:** Transfers issues from Jira to GitLab, preserving states, comments, and attachments.
- **User Mapping:** Maps Jira users to GitLab users, maintaining assignment and ownership.
- **Custom Field Handling:** Converts Jira custom fields into GitLab labels or issue notes, as applicable.
- **Workflow Adaptation:** Translates Jira workflows into GitLab issue boards and milestones.

## Prerequisites

Before you begin the migration process, ensure you have:

- Administrative access to your Jira instance.
- A GitLab account with adequate permissions to create groups and projects.
- The `jira2gitlab` tool installed on a system with network access to both Jira and GitLab.

## Installation

1. Clone the repository:
   ```
   git clone https://gitlab.com/gitlab-org/professional-services-automation/tools/migration/jira2gitlab.git
   ```
2. Navigate to the tool's directory:
   ```
   cd jira2gitlab
   ```
3. Install the dependencies.
   ```
   poetry install
   ```

## Usage

1. Get inside the `src` folder and configure the tool using the `jira2gitlab_config.py` file, specifying your Jira and GitLab instance details and migration preferences. Update the `jira2gitlab_secrets.py` file with your authentication credentials.

2. List all possible resources from the Jira Server in configuration file:
   ```
   poetry run python3 jira2gitlab_list.py
   ```
3. Migrate Users:
   ```
   poetry run python3 jira2gitlab_migrate_users.py
   ```
4. Migrate Projects:
   ```
   poetry run python3 jira2gitlab_migrate_projects.py
   ```
5. Monitor the output for any errors or warnings during the migration process.

## Configuration

Detailed information on configuring the tool can be found in the [Jira Migration Delivery kit](https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/jira-importer). This includes specifying project mappings, user mappings, and handling of custom fields.

## Support

For support, questions, or contributions to the Jira2Gitlab Migration Tool, please open an issue in this repository.

## Contributing

Contributions are welcome! Please see the `contributing.md` file for how to contribute to this project.
