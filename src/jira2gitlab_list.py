
import requests
import logging
import random

from jira2gitlab_secrets import *
from jira2gitlab_config import *

# Setup logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')


headers = {
    'Content-Type': 'application/json',
    'Authorization': f'Bearer {JIRA_TOKEN}'
}

# Function to get projects
def get_projects():
    logging.info("Fetching projects from Jira")
    response = requests.get(f'{JIRA_API}/project', headers=headers)
    response.raise_for_status()
    data = response.json()
    logging.info(f"Fetched {len(data)} projects")
    return response.json()

# Function to get users
def get_users():
    users = []
    start_at = 0
    max_results = 50
    logging.info("Fetching users from Jira")

    while True:
        response = requests.get(f'{JIRA_API}/user/search', params={'startAt': start_at, 'maxResults': max_results, 'username': '.', 'includeInactive':'true'}, headers=headers)
        if response.status_code == 200:
            data = response.json()
            users.extend(data)
            logging.info(f"Fetched {len(data)} users")
            if len(data) < max_results:
                break
            start_at += len(data)
        else:
            logging.error(f"Failed to fetch users: {response.status_code} {response.text}")
            break

    return users

# Function to fetch issue types
def get_issue_types():
    response = requests.get(f'{JIRA_API}/issuetype', headers=headers)
    if response.status_code == 200:
        issue_types = response.json()
        logging.info("Successfully fetched issue types")
        return issue_types
    else:
        logging.error(f"Failed to fetch issue types: {response.status_code} {response.text}")
        return []

# Function to fetch issue priorities
def get_issue_priorities():
    response = requests.get(f'{JIRA_API}/priority', headers=headers)
    if response.status_code == 200:
        priorities = response.json()
        logging.info("Successfully fetched issue priorities")
        return priorities
    else:
        logging.error(f"Failed to fetch issue priorities: {response.status_code} {response.text}")
        return []

# Function to fetch issue resolutions
def get_issue_resolutions():
    response = requests.get(f'{JIRA_API}/resolution', headers=headers)
    if response.status_code == 200:
        resolutions = response.json()
        logging.info("Successfully fetched issue resolutions")
        return resolutions
    else:
        logging.error(f"Failed to fetch issue resolutions: {response.status_code} {response.text}")
        return []

# Function to fetch issue statuses
def get_issue_statuses():
    response = requests.get(f'{JIRA_API}/status', headers=headers)
    if response.status_code == 200:
        statuses = response.json()
        logging.info("Successfully fetched issue statuses")
        return statuses
    else:
        logging.error(f"Failed to fetch issue statuses: {response.status_code} {response.text}")
        return []

def generate_random_color():
    return "#{:06x}".format(random.randint(0, 0xFFFFFF))

# Fetch data
projects = get_projects()
users = get_users()
issue_types = get_issue_types()
issue_priorities = get_issue_priorities()
issue_resolutions = get_issue_resolutions()
issue_statuses = get_issue_statuses()

# Prepare the dictionaries
new_projects_dict = {project['name']: f"{PROJECTS_NAMESPACE}/{project['name']}" for project in projects}
new_users_dict = {user['name']: user['name'].replace(" ", ".").replace("@","_").lower() for user in users}
new_issue_types_dict = {issue_type['name']: f"type::{issue_type['name'].lower()}" for issue_type in issue_types}
new_issue_priorities_dict = {priority['name']: f"priority::{priority['id']}" for priority in issue_priorities}
new_issue_resolutions_dict = {resolution['name']: f"resolution::{resolution['name']}" for resolution in issue_resolutions}
new_issue_statuses_dict = {status['name']: f"status::{status['name']}" for status in issue_statuses}

# Collect all unique values from the maps
unique_values = set()
for map_ in (new_issue_types_dict, new_issue_priorities_dict, new_issue_resolutions_dict, new_issue_statuses_dict):
    unique_values.update(map_.values())

# Update the dictionaries in the target file
dict_file_path = 'jira2gitlab_config.py'
with open(dict_file_path, 'r') as file:
    lines = file.readlines()

# Build LABEL_COLORS dictionary with random colors for each unique value
new_label_colors_dict = {value: generate_random_color() for value in unique_values}


# Find and update dicts
project_line_found = False
user_map_line_found = False
issue_type_map_line_found = False
issue_priority_map_line_found = False
issue_resolutions_map_line_found = False
issue_statuses_map_line_found = False
label_colors_map_line_found = False
for i, line in enumerate(lines):
    if 'PROJECTS = {' in line:
        lines[i] = f'PROJECTS = {new_projects_dict}\n'
        project_line_found = True
    if 'USER_MAP = {' in line:
        lines[i] = f'USER_MAP = {new_users_dict}\n'
        user_map_line_found = True
    if 'ISSUE_TYPE_MAP = {' in line:
        lines[i] = f'ISSUE_TYPE_MAP = {new_issue_types_dict}\n'
        issue_type_map_line_found = True
    if 'ISSUE_PRIORITY_MAP = {' in line:
        lines[i] = f'ISSUE_PRIORITY_MAP = {new_issue_priorities_dict}\n'
        issue_priority_map_line_found = True
    if 'ISSUE_RESOLUTION_MAP = {' in line:
        lines[i] = f'ISSUE_RESOLUTION_MAP = {new_issue_resolutions_dict}\n'
        issue_resolutions_map_line_found = True
    if 'ISSUE_STATUS_MAP = {' in line:
        lines[i] = f'ISSUE_STATUS_MAP = {new_issue_statuses_dict}\n'
        issue_statuses_map_line_found = True
    if 'LABEL_COLORS = {' in line:
        lines[i] = f'LABEL_COLORS = {new_label_colors_dict}\n'
        label_colors_map_line_found = True
    if project_line_found and user_map_line_found and issue_type_map_line_found and issue_priority_map_line_found and issue_resolutions_map_line_found and issue_statuses_map_line_found and label_colors_map_line_found:
        break

# Write the changes back to the file
with open(dict_file_path, 'w') as file:
    file.writelines(lines)

print("Data listed successfully.")
