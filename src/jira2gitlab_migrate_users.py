
# Improved upon https://gist.github.com/Gwerlas/980141404bccfa0b0c1d49f580c2d494

# Jira API documentation : https://docs.atlassian.com/software/jira/docs/api/REST/8.5.1/
# Gitlab API documentation: https://docs.gitlab.com/ee/api/README.html

import re
import sys
import uuid
import json
import pickle
import hashlib
import urllib3
import urllib.parse
import unicodedata
import traceback
import signal
import requests
import logging
import time

from requests.auth import HTTPBasicAuth
from io import BytesIO
from pathlib import Path
from typing import Dict, Any
from multiprocessing import Manager, Pool

from label_colors import create_or_update_label_colors
from jira2gitlab_secrets import *
from jira2gitlab_config import *
from gitlab_ps_utils.processes import MultiProcessing


### set library defaults
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# increase the number of retry connections
requests.adapters.DEFAULT_RETRIES = 10

# Setup logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# close redundant connections
# requests uses the urllib3 library, the default http connection is keep-alive, requests set False to close.
s = requests.session()
s.keep_alive = False

# Change admin role of Gitlab users
def gitlab_user_admin(user, admin, import_status):
    # Cannot change root's admin status
    if user['username'] == GITLAB_ADMIN:
        return user

    try:
        gl_user = requests.put(
            f"{GITLAB_API}/users/{user['id']}",
            headers = {'PRIVATE-TOKEN': GITLAB_TOKEN},
            verify = VERIFY_SSL_CERTIFICATE,
            json = { 'admin': admin }
        )
        gl_user.raise_for_status()
    except requests.exceptions.RequestException as e:
        raise Exception(f"Unable change admin status of Gilab user {user['username']} to {admin}\n{e}")
    gl_user = gl_user.json()
    
    if admin:
        import_status['gl_users_made_admin'].add(gl_user['username'])
    else:
        import_status['gl_users_made_admin'].remove(gl_user['username'])
    
    return gl_user

# Migrate a user
def migrate_user(jira_username, gitlab_username, import_status, gl_users):
    logging.info(f"Migrating user: {jira_username}")

    if jira_username == 'jira':
        return gl_users[GITLAB_ADMIN]
    
    if jira_username == 'admin':
        return gl_users[GITLAB_ADMIN]

    try:
        jira_user = requests.get(
            f'{JIRA_API}/user?username={jira_username}',
            verify = VERIFY_SSL_CERTIFICATE,
            headers={
                'Content-Type': 'application/json',
                'Authorization': f'Bearer {JIRA_TOKEN}'
            },
        )
        jira_user.raise_for_status()
    except requests.exceptions.RequestException as e:
        raise Exception(f"Unable to read {jira_username} from Jira!\n{e}")
    jira_user = jira_user.json()

    headers = {
    "Private-Token": GITLAB_TOKEN,
    "Content-Type": "application/json",
    }

    data = {
        "email": jira_user['emailAddress'],
        "password": NEW_GITLAB_USERS_PASSWORD,
        "username": gitlab_username,
        "name": jira_user['displayName'],
        "admin": MAKE_USERS_TEMPORARILY_ADMINS,
    }

    searchResponse = requests.get(f'{GITLAB_API}/users?search={data["email"]}', headers=headers)
    if searchResponse.status_code == 200 and searchResponse.json():
        logging.info(f"A user with the email {data['email']} already exists.")
        gl_user = searchResponse.json()[0]
    else:
        try:
            postResponse = requests.post(f'{GITLAB_API}/users', headers=headers, verify = VERIFY_SSL_CERTIFICATE, json=data)
            postResponse.raise_for_status()
            logging.info(f"User {jira_username} created successfully.")
        except requests.exceptions.HTTPError as err:
            logging.error(f"HTTP error occurred: {err}")
            logging.error(f"Response body: {postResponse.text}")
        except requests.exceptions.RequestException as e:
            logging.error(f"Request error occurred: {e}")
        gl_user = postResponse.json()

    if MAKE_USERS_TEMPORARILY_ADMINS:
        import_status['gl_users_made_admin'].add(gl_user['username'])

    gl_users[gl_user['username']] = gl_user

    return gl_user


def store_import_status(import_status):
    with open('import_status.pickle', 'wb') as f:
        pickle.dump(import_status, f, pickle.HIGHEST_PROTOCOL)

def load_import_status():
    try:
        with open('import_status.pickle', 'rb') as f:
            import_status = pickle.load(f)
    except:
        print("[INFO]: Creating new import_status file")
        import_status = {
            'issue_mapping': dict(),
            'gl_users_made_admin' : set(),
            'links_todo' : set()
        }
    return import_status

def get_available_users():
    gl_users = dict()
    page = 1
    while True:
        rq = requests.get(
            f'{GITLAB_API}/users?page={str(page)}', 
            headers = {'PRIVATE-TOKEN': GITLAB_TOKEN},
            verify = VERIFY_SSL_CERTIFICATE
        )
        rq.raise_for_status()
        for gl_user in rq.json():
            gl_users[gl_user['username']] = gl_user
        if (rq.headers["x-page"] != rq.headers["x-total-pages"]):
            page = rq.headers["x-next-page"] 
        else:
            break
    return gl_users

# Translate types that the json module cannot encode
def json_encoder(obj):
    if isinstance(obj, set):
        return list(obj)

################################################################
# Main body
################################################################

# Users that were made admin during the import need to be changed back
def reset_user_privileges(import_status):
    print('\nResetting user privileges..\n')
    for gl_username in import_status['gl_users_made_admin'].copy():
        print(f"- User {gl_users[gl_username]['username']} was made admin during the import to set the correct timestamps. Turning it back to non-admin.")
        gitlab_user_admin(gl_users[gl_username], False, import_status)
    assert (not import_status['gl_users_made_admin'])

def final_report(import_status):
    if jira_users_not_mapped:
        print(f"\nThe following Jira users could not be mapped to Gitlab. They have been impersonated by {GITLAB_ADMIN} (number of times):")
        print(f"{json.dumps(jira_users_not_mapped, default=json_encoder, indent=4)}\n")

    if gl_users_not_migrated:
        print(f"\nThe following Jira users could not be found in Gitlab and could not be migrated. They have been impersonated by {GITLAB_ADMIN} (number of times)")
        print(f"{json.dumps(gl_users_not_migrated, default=json_encoder, indent=4)}\n")

    if import_status['gl_users_made_admin']:
        print("An error occurred while reverting the admin status of Gitlab users.")
        print("IMPORTANT: The following users should be revoked the admin status manually:")
        print(f"{json.dumps(import_status['gl_users_made_admin'], default=json_encoder, indent=4)}\n")

class SigIntException(Exception):
    pass

def wrapup(import_status, import_success):
    if import_success:
        print("\n\nMigration completed successfully\n")
    else:
        (exctype,_,_) = sys.exc_info()
        if exctype != SigIntException:
            traceback.print_exc()
        print("\n\nMigration failed\n")

    # Users that were made admin during the import need to be changed back
    try:
        reset_user_privileges(import_status)
    except Exception as e:
        print(f"\n[ERROR] Could not reset privileges: {e}\n")

    store_import_status(import_status)

    final_report(import_status)

    if not import_success:
        exit(1)

def sigint_handler(signum, frame):
    print("\n\nMigration interrupted (SIGINT)\n")
    raise SigIntException

def migrate_users(import_status, gl_users, user):
    jira_user, gitlab_user = user
    logging.info("Migrating users from Jira to Gitlab")
    migrate_user(jira_user, gitlab_user, import_status, gl_users)


# register SIGINT handler, to catch interruptions and wrap up gracefully
signal.signal(signal.SIGINT, sigint_handler)

IMPORT_SUCCEEDED = False

if __name__ == "__main__":
    if Path(IMPORT_STATUS_FILENAME).exists():
        continue_pickle = input("Pickle file exists, continue? (y/n)\n")
        if continue_pickle in "nN":
            sys.exit(1)

    # Record start time
    start_time = time.time()

    # Get available Gitlab users
    gl_users = get_available_users()

    # Jira users that could not be mapped to Gitlab users
    jira_users_not_mapped = dict()

    # Gitlab users that were mapped to, but could not be migrated
    gl_users_not_migrated = dict()

    # Load previous import status
    import_status = load_import_status()

    try:
        # Init multiprocessing class
        multi = MultiProcessing()

        # Migrate users
        if MIGRATE_USERS:
            multi.start_multi_process_stream_with_args(migrate_users, USER_MAP.items(), import_status, gl_users, processes=10)

        IMPORT_SUCCEEDED = True
    finally:
        wrapup(import_status, IMPORT_SUCCEEDED)

    # Record end time
    end_time = time.time()

    # Calculate and print the execution time
    execution_time = end_time - start_time
    print(f"Migration of users executed in {execution_time} seconds.")
