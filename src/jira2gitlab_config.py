################################################################
# Jira options
################################################################

JIRA_URL = 'http://example.jira.server'
JIRA_API = f'{JIRA_URL}/rest/api/2'

# Bitbucket URL, if available, is only used in pattern-matching
# to translate issue references to commits.
BITBUCKET_URL = 'https://bitbucket.example.com'

# How many items to request at a time from Jira (usually not more than 1000)
JIRA_PAGINATION_SIZE = 25

# the Jira Epic custom field
JIRA_EPIC_FIELD = 'customfield_10103'

# the Jira Sprints custom field
JIRA_SPRINT_FIELD = 'customfield_10340'

# the Jira story points custom field
JIRA_STORY_POINTS_FIELD = 'customfield_10002'

# Custom JIRA fields
JIRA_CUSTOM_FIELDS = {
    # 'customfield_14200': 'Metadata 1',
    # 'customfield_14201': 'Metadata 2',
}

################################################################
# Gitlab options
################################################################

GITLAB_URL = 'http://example.gitlab.com'
GITLAB_API = f'{GITLAB_URL}/api/v4'

# Support Gitlab Premium features (e.g. epics and "blocks" issue links)
GITLAB_PREMIUM = True

################################################################
# Import options
################################################################

# Name of the file storing the status of imports
IMPORT_STATUS_FILENAME = 'import_status.pickle'

# Set this to false if JIRA / Gitlab is using self-signed certificate.
VERIFY_SSL_CERTIFICATE = False

# PREFIX_LABEL is used with all existing Jira labels
PREFIX_LABEL = ''

# PREFIX_COMPONENT is used with existing Jira components when no match is found in ISSUE_COMPONENT_MAP
# NOTE: better NOT to use a prefix for components, otherwise only 1 component will be imported in Gitlab
PREFIX_COMPONENT = ''

# PREFIX_PRIORITY is used with existing Jira priorities when no match is found in ISSUE_PRIORITY_MAP
PREFIX_PRIORITY = 'P::'

# Whether to migrate issue attachments
MIGRATE_ATTACHMENTS = True

# Whether to migrate worklogs as issue comment with /spend quick-action.
MIGRATE_WORLOGS = True

# Jira users are mapped to Gitlab users according to USER_MAP, with the following two exceptions:
# - Jira user 'jira' is mapped to Gitlab user 'root'
# - Jira users that are not in USER_MAP are mapped to Gitlab user 'root'
# If MIGRATE_USERS is True, mapped Gitlab users that don't exist yet in Gitlab will be migrated automatically
# If MIGRATE_USERS is False, all actions performed by a non-existing Gitlab user will be performed by Gitlab user 'root'
MIGRATE_USERS = False

# When MIGRATE_USERS is True, new users can be created in Gitlab.
# This is the *temporary* password they get.
# It must not contain commonly used combinations of words and letters
# And this should be changed by all users login for the first time
NEW_GITLAB_USERS_PASSWORD = 'vtUqsxwHvWQk'

# If (new or existing) Gitlab users are not made admins during the import,
# the original timestamps of all user actions cannot be imported. Instead, the timestamp of the import will be used.
# When this option is enabled, users are made admin and changed back to their original role after the import. 
# If users cannot be changed back to non-admin, this is reported at the end of the import.
# This feature is recommended, but to be used with caution. Check users' status in Gitlab after the import.
MAKE_USERS_TEMPORARILY_ADMINS = True

# Prefix issue titles with "[PROJ-123]" (Jira issue-key)
ADD_JIRA_KEY_TO_TITLE = True

# REFERECE_BITBUCKET_COMMITS = True -> tries to translate Jira issue references in Bitbucket to Gitlab issue references
# Disable if the Jira instance does not have an active link to Bitbucket at the moment of the import
# Disable if not needed, to increase performance (more calls are needed for each issue)
# Limitations:
# - Bitbucket repositories need to be imported in Gitlab with the same project name (the group name can change)
# - This feature only works if the issue project and the commit project are in the same Gitlab group
REFERECE_BITBUCKET_COMMITS = True

# Try force converting broken jira tables (tables that have no headers)
FORCE_REPAIR_JIRA_TABLES = False

# Set this to True if you want to keep original attachments filenames.
# Diacritics are removed, but no full normalisation to ASCII is performed.
# Therefore this may cause 500 errors on some unicode characters.
# When set to False, filenames are replaced with UUIDs.
KEEP_ORIGINAL_ATTACHMENT_FILENAMES = True

################################################################
# Import mappings
################################################################

# Name of the default group where we import projects that are listed automatically
PROJECTS_NAMESPACE = 'project_namespace'

# Jira - Gitlab group/project mapping
# Groups are not created. They must already exist in Gitlab.
# You can change manually the name of the group where the project will be imported from this collection
PROJECTS = {'Project 1': 'project_namespace/Project 1', 'Project 2': 'project_namespace/Project 2'}

# Bitbucket - Gitlab mapping
# *Not* used to migrate Bitbucket repos (use Gitlab's integration for that)
# Used to map references from issues to commits in Bitbucket repos that are migrated to Gitlab
# Make sure you use the correct casing for Bitbucket: project key is all upper-case, repository is all lower-case
PROJECTS_BITBUCKET = {
  'PROJ1/repository1': 'group1/project1',
  'PROJ2/repository2': 'group1/project2',
}

# Jira - Gitlab username mapping
USER_MAP = {'alice.doe': 'alice.doe', 'john.doe': 'john.doe'}

# Map Jira issue types to Gitlab labels
# Unknown issue types are mapped as generic labels
ISSUE_TYPE_MAP = {'Epic': 'type::epic', 'User Story': 'type::user story', 'Bug': 'type::bug'}

# Map Jira components to labels
# NOTE: better NOT to use a prefix for components, otherwise only 1 component will be imported in Gitlab
ISSUE_COMPONENT_MAP = {
    'Component1': 'component1',
    'Component2': 'component2'
}

# Map Jira priorities to labels
ISSUE_PRIORITY_MAP = {'Blocker (1)': 'priority::1', 'Critical (2)': 'priority::2', 'High (3)': 'priority::3', 'Normal (4)': 'priority::4'}

# Map Jira resolutions to labels
ISSUE_RESOLUTION_MAP = {'Fixed': 'resolution::Fixed', "Won't Fix": "resolution::Won't Fix"}

# Map Jira statuses to labels
ISSUE_STATUS_MAP = {'Open': 'status::Open', 'In Progress': 'status::In Progress', 'Reopened': 'status::Reopened'}

# These Jira statuses will cause the corresponding Gitlab issue to be closed
ISSUE_STATUS_CLOSED = {
  'Done',
  'Closed',
  'Completed',
  'Resolved'
}

# Set colors for single labels or group of labels
LABEL_COLORS = {'type::feature set': '#80ce3b', 'status::Escalated': '#0a3422'}
